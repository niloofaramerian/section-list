package com.example.sectionedlist

import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.intrusoft.sectionrecyclerview.SectionItem
import com.intrusoft.sectionrecyclerview.SectionRecyclerViewAdapter



class AdapterSectionRecycler(var context: Context, var sections: List<Sections>, recyclerView: RecyclerView?, sectionResId: Int, childResId: Int , val itemClick : (Childs) -> Unit)
    :
    SectionRecyclerViewAdapter<HolderView>(context, sections, recyclerView, sectionResId, childResId) {

    override fun onCreateSectionViewHolder(fullSectionView: View, sectionView: View, child: View): HolderView {

        if (child == null) {
            Toast.makeText(context, "NULL CHILD", Toast.LENGTH_SHORT).show()
        }
        return HolderView(fullSectionView, sectionView, child)
    }

    override fun onBindSectionView(sectionHolder: HolderView, position: Int, sectionItem: SectionItem) {

        sectionHolder.sectionText.text = sections[position].sectionText
    }

    override fun onBindChildView(position: Int, view: View, childItem: Any): View {

        val child = childItem as Childs
        val childPersianText = view.findViewById<View>(R.id.main_textView_persian_bird_name) as TextView
        val childEngText= view.findViewById<View>(R.id.main_textView_scientific_bird_name) as TextView
        val childImage = view.findViewById<View>(R.id.main_imageView_bird) as ImageView
        childPersianText.text = child.persianName
        childEngText.text = child.scientificName
        val resourceId = context.resources.getIdentifier(child.image , "drawable" , context.packageName)
        childImage.setImageResource(resourceId)
        view.setOnClickListener {
            itemClick(child)
        }
        return view
    }

}