package com.example.sectionedlist

import android.view.View
import android.widget.TextView
import com.intrusoft.sectionrecyclerview.SectionViewHolder


class HolderView(fullSectionView: View?, sectionView: View, childs: View?) : SectionViewHolder(fullSectionView, sectionView, childs) {
    var sectionText: TextView

    init {
        sectionText = sectionView.findViewById<TextView>(R.id.main_textView_section)
    }
}