package com.example.sectionedlist

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class MainActivity : AppCompatActivity() {

    var recyclerView: RecyclerView? = null
    var rcvn : RecyclerView?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView = findViewById(R.id.main_recycler_view)
        val layoutManager = LinearLayoutManager(this)
        recyclerView!!.layoutManager = layoutManager

        var childsList: MutableList<Childs> = ArrayList()
        childsList.add(Childs("bird9_1" , "آب شکاف" , "black skimmer"))
        childsList.add(Childs("bird9_2" ,"ابیا" , "eurasian"))
        childsList.add(Childs("bird9_5" , "آب چیلک تالابی" , "marsh sandiper"))


        val sections: MutableList<Sections> = ArrayList()
        sections.add(Sections(childsList, "بچیلک (charadriiformes)"))

        childsList = ArrayList()
        childsList.add(Childs("bird15_2" , "تلیه کوچک" , "gtan sandipi"))
        childsList.add(Childs("bird15_3" , "تلیه بزرگ" ,"wood sandipjjb"))
        childsList.add(Childs("bird15_4" , "آبچیلک خالدار", "abchilak"))
        sections.add(Sections(childsList, "اسکوا ( strecorardii )"))



        val adapter = AdapterSectionRecycler(this , sections , recyclerView , R.layout.recycler_view_section_layout , R.layout.recycler_view_item_layout){
            val intent = Intent(this , MainActivity2::class.java)
            intent.putExtra("text1" ,it.persianName )
            startActivity(intent)
        }
        recyclerView!!.adapter = adapter

    }


}